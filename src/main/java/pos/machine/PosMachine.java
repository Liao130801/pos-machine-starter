package pos.machine;

import java.util.*;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        return generateRecords(buildItemList(barcodes));
    }
    public static Map<String, Integer> calculatedQuantity(List<String> barcodes){
        Map<String, Integer> countMap = new HashMap<>();
        for (String barcode : barcodes) {
            countMap.put(barcode, countMap.getOrDefault(barcode, 0) + 1);
        }
        return countMap;
    }
    public static List<String> duplicateList(List<String> barcodes){
        Collection collection = new LinkedHashSet(barcodes);
        List list = new ArrayList(collection);
        return list;
    }
    public List<ReciptItem>  buildItemList(List<String> barcodes){
        Map<String, Integer> countMap = calculatedQuantity(barcodes);
        List<ReciptItem> reciptItemList = new ArrayList<>();
        List<String> duplicate= duplicateList(barcodes);
        for (String barcode : duplicate) {
            Item item=selectItem(barcode);
            ReciptItem reciptItem = new ReciptItem(item.getBarcode(),item.getName(),item.getPrice(), countMap.get(barcode));
            reciptItemList.add(reciptItem);
        }
        return reciptItemList;
    }
    public static Item selectItem(String barcode){
        ItemsLoader itemsLoader =new ItemsLoader();
        List<Item> itemList = itemsLoader.loadAllItems();
        for (Item item : itemList) {
            if(item.getBarcode().equals(barcode))return item;
        }
        return null;
    }
    public static String generateRecords(List<ReciptItem> reciptItemList){
        StringBuilder generateRecords = new StringBuilder();
        StringBuilder itemRecords = new StringBuilder();
        int total=0;
        for(ReciptItem reciptItem : reciptItemList){
            total+=reciptItem.getPrice()*reciptItem.getQuantity();
            itemRecords.append(generateLine(reciptItem)).append("\n");
        }
        return generateRecords.append("***<store earning no money>Receipt***").append("\n").append(itemRecords).append("----------------------").append("\n")
                .append("Total: ")
                .append(total).append(" (yuan)").append("\n").append("**********************").toString();
    }
    public static String generateLine(ReciptItem reciptItem){
        StringBuilder generateLine = new StringBuilder();
        return generateLine.append("Name: ").append(reciptItem.getName()).append(", ").append("Quantity: ").append(reciptItem.getQuantity()).append(", ")
                .append("Unit price: ").append(reciptItem.getPrice()).append(" (yuan)").append(", ").append("Subtotal: ").append(reciptItem.getPrice()*reciptItem.getQuantity())
                .append(" (yuan)").toString();
    }
}
